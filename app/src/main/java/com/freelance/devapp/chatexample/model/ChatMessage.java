package com.freelance.devapp.chatexample.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by jedsada-pc on 1/7/2559.
 */
public class ChatMessage implements Parcelable {

    private String urlMine, urlOther;
    private boolean isMine;
    private String content;
    private String timeStamp;

    public ChatMessage(String message, boolean mine, String timeStamp, String url) {
        this.content = message;
        this.isMine = mine;
        this.timeStamp = timeStamp;
        if (mine)
            urlMine = url;
        else
            urlOther = url;
    }

    protected ChatMessage(Parcel in) {
        urlMine = in.readString();
        urlOther = in.readString();
        isMine = in.readByte() != 0;
        content = in.readString();
        timeStamp = in.readString();
    }

    public static final Creator<ChatMessage> CREATOR = new Creator<ChatMessage>() {
        @Override
        public ChatMessage createFromParcel(Parcel in) {
            return new ChatMessage(in);
        }

        @Override
        public ChatMessage[] newArray(int size) {
            return new ChatMessage[size];
        }
    };

    public String getUrlMine() {
        return urlMine;
    }

    public void setUrlMine(String urlMine) {
        this.urlMine = urlMine;
    }

    public String getUrlOther() {
        return urlOther;
    }

    public void setUrlOther(String urlOther) {
        this.urlOther = urlOther;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isMine() {
        return isMine;
    }

    public void setIsMine(boolean isMine) {
        this.isMine = isMine;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(urlMine);
        parcel.writeString(urlOther);
        parcel.writeByte((byte) (isMine ? 1 : 0));
        parcel.writeString(content);
        parcel.writeString(timeStamp);
    }
}
