package com.freelance.devapp.chatexample.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.freelance.devapp.chatexample.R;
import com.freelance.devapp.chatexample.model.ChatMessage;

import java.util.LinkedList;
import java.util.List;

import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by Alessandro Barreto on 23/06/2016.
 */
public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MyChatViewHolder> {

    private static final int MY_MESSAGE = 0, OTHER_MESSAGE = 1;
    private List<ChatMessage> messageList = new LinkedList<>();
    private Activity activity;

    public ChatAdapter(Activity activity, List<ChatMessage> messageList) {
        this.activity = activity;
        this.messageList = messageList;
    }

    @Override
    public MyChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout = -1;
        switch (viewType) {
            case MY_MESSAGE:
                layout = R.layout.item_message_right;
                break;
            case OTHER_MESSAGE:
                layout = R.layout.item_message_left;
                break;
        }
        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(layout, parent, false);
        return new MyChatViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyChatViewHolder holder, int position) {
        holder.setTvTimestamp(messageList.get(position).getTimeStamp());
        holder.setTxtMessage(messageList.get(position).getContent());
        if (messageList.get(position).isMine())
            holder.setIvUser(messageList.get(position).getUrlMine());
        else
            holder.setIvOther(messageList.get(position).getUrlOther());
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return messageList.get(position).isMine() ? 0 : 1;
    }

    public void updateData(List<ChatMessage> data) {
        this.messageList = data;
        this.notifyDataSetChanged();
    }

    public class MyChatViewHolder extends RecyclerView.ViewHolder {

        private TextView tvTimestamp;
        private EmojiconTextView txtMessage;
        private ImageView ivUser, ivChatPhoto;

        public MyChatViewHolder(View itemView) {
            super(itemView);
            tvTimestamp = (TextView) itemView.findViewById(R.id.timestamp);
            txtMessage = (EmojiconTextView) itemView.findViewById(R.id.txtMessage);
            ivUser = (ImageView) itemView.findViewById(R.id.ivUserChat);
            ivChatPhoto = (ImageView) itemView.findViewById(R.id.ivChat);
        }

        public void setTxtMessage(String message) {
            if (txtMessage == null) return;
            txtMessage.setText(message);
        }

        public void setIvUser(String urlImageMine) {
            if (ivUser == null) return;
            Glide.with(ivUser.getContext())
                    .load(urlImageMine)
                    .centerCrop()
                    .transform(new CircleTransform(ivUser.getContext()))
                    .override(40, 40)
                    .into(ivUser);
        }

        public void setTvTimestamp(String timestamp) {
            if (tvTimestamp == null) return;
            tvTimestamp.setText(convertTimestamp(timestamp));
        }

        public void setIvOther(String urlImageOther) {
            if (ivChatPhoto == null) return;
            Glide.with(ivChatPhoto.getContext())
                    .load(urlImageOther)
                    .override(40, 40)
                    .centerCrop()
                    .transform(new CircleTransform(ivChatPhoto.getContext()))
                    .into(ivChatPhoto);
        }
    }

    private CharSequence convertTimestamp(String mileSegundos) {
        return DateUtils.getRelativeTimeSpanString(Long.parseLong(mileSegundos), System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
    }
}
