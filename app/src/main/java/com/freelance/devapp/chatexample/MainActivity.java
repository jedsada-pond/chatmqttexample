package com.freelance.devapp.chatexample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.freelance.devapp.chatexample.adapter.ChatAdapter;
import com.freelance.devapp.chatexample.model.ChatMessage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String HOST = "tcp://broker.mqtt-dashboard.com:1883";
    private RecyclerView rvListMessage;
    private LinearLayoutManager mLinearLayoutManager;
    private ImageView btSendMessage, btEmoji;
    private EmojiconEditText edMessage;
    private View contentRoot;
    private EmojIconActions emojIcon;
    private ChatAdapter adapter;
    private List<ChatMessage> data = new LinkedList<>();
    private MqttAndroidClient mqttAndroidClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindView();
        initMQTT();
    }

    private void bindView() {
        contentRoot = findViewById(R.id.contentRoot);
        edMessage = (EmojiconEditText) findViewById(R.id.editTextMessage);
        btSendMessage = (ImageView) findViewById(R.id.buttonMessage);
        btSendMessage.setOnClickListener(this);
        btEmoji = (ImageView) findViewById(R.id.buttonEmoji);
        emojIcon = new EmojIconActions(this, contentRoot, edMessage, btEmoji);
        emojIcon.ShowEmojIcon();
        rvListMessage = (RecyclerView) findViewById(R.id.messageRecyclerView);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setStackFromEnd(true);
        rvListMessage.setLayoutManager(mLinearLayoutManager);
        adapter = new ChatAdapter(MainActivity.this, data);
        rvListMessage.setAdapter(adapter);
    }

    private void initMQTT() {
        mqttAndroidClient = new MqttAndroidClient(MainActivity.this, HOST, MqttClient.generateClientId());
        mqttAndroidClient.setCallback(mqttCallback);
        connectMQTT();
    }

    private void connectMQTT() {
        try {
            mqttAndroidClient.connect(null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Toast.makeText(getApplicationContext(), "Connection Success!", Toast.LENGTH_SHORT).show();
                    try {
                        /**
                         * device one subscribe : message_other
                         * device two subscribe : message_mine
                         */
//                        mqttAndroidClient.subscribe("message_other", 0);
                        mqttAndroidClient.subscribe("message_mine", 0);
                    } catch (MqttException ex) {
                        ex.printStackTrace();
                    }
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Log.d("mqttAndroidClient", "Connection Failure!" + "\t" + exception.getMessage());
                    connectMQTT();
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    final MqttCallback mqttCallback = new MqttCallback() {
        @Override
        public void connectionLost(Throwable cause) {
            Log.d("mqttCallback", "Connection was lost!");
            connectMQTT();
        }

        @Override
        public void messageArrived(String topic, MqttMessage message) throws Exception {
            ChatMessage chatMessage1 = new ChatMessage(message.toString(), false
                    , Calendar.getInstance().getTime().getTime() + ""
                    , "http://static.goal.com/2924800/2924812_heroa.jpg");
            updateChat(chatMessage1);
        }

        @Override
        public void deliveryComplete(IMqttDeliveryToken token) {
            Log.d("mqttCallback", "Delivery Complete!");
        }
    };

    private void updateChat(ChatMessage chatMessage) {
        data.add(chatMessage);
        rvListMessage.scrollToPosition(adapter.getItemCount() - 1);
        adapter.updateData(data);
    }

    private void updateChat() {
        rvListMessage.scrollToPosition(adapter.getItemCount() - 1);
        adapter.updateData(data);
    }

    private void sendMessage() {
        if (edMessage.getText().toString().isEmpty())
            return;

        MqttMessage mqttMessage = new MqttMessage();
        mqttMessage.setPayload(edMessage.getText().toString().getBytes());
        try {
            /**
             * device one publish : message_mine
             * device two publish : message_other
             */
//            mqttAndroidClient.publish("message_mine", mqttMessage);
            mqttAndroidClient.publish("message_other", mqttMessage);
        } catch (MqttException e) {
            e.printStackTrace();
        }

        ChatMessage chatMessage = new ChatMessage(edMessage.getText().toString(), true
                , Calendar.getInstance().getTime().getTime() + ""
                , "http://www.lr21.com.uy/wp-content/uploads/2016/06/messi-columna.jpg");
        edMessage.setText(null);
        updateChat(chatMessage);
    }

    @Override
    protected void onDestroy() {
        try {
            if (mqttAndroidClient.isConnected()) {
                mqttAndroidClient.unregisterResources();
                mqttAndroidClient.close();
                mqttAndroidClient.disconnect();
            }
        } catch (MqttException e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonMessage:
                sendMessage();
                break;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        String json = new Gson().toJson(data);
        outState.putString("json", json);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        String json = savedInstanceState.getString("json");
        if (!TextUtils.isEmpty(json)) {
            data = new Gson().fromJson(json, new TypeToken<List<ChatMessage>>() {
            }.getType());
            updateChat();
        }
    }
}
